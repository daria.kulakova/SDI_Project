/*
* Registers.h
*
* Created: 11/23/2016 11:18:01 AM
* Author: Roman
*/


#ifndef __REGISTERS_H__
#define __REGISTERS_H__

#include <asf.h>

class Registers
{
	//variables
	public:
	uint8_t GENERAL_CONTROL_REGISTER_ADDRESS[1] = {0x00};
		uint8_t CARRIER_DETECT[1] = {0x10};
		uint8_t MUTE[1] = {0x20};
		uint8_t BYPASS[1] = {0x30};
		uint8_t SLEEP_MODE[1] = {0x40};
		uint8_t EXTENDED_3G_REACH_MODE[1] = {0x50};

	uint8_t OUTPUT_DRIVER_REGISTER_ADDRESS[1] = {0x01};
		uint8_t OUTPUT_SWING[1] = {0x11};
		uint8_t OFFSET_VOLTAGE[1] = {0x21};

	uint8_t LAUNCH_AMPLITUDE_REGISTER_ADDRESS[1] = {0x02};
		uint8_t COARSE_CONTROL[1] = {0x12};
		uint8_t FINE_CONTROL[1] = {0x22};

	uint8_t CLI_REGISTER_ADDRESS[1] = {0x03};

	uint8_t DEVICE_ID_REGISTER_ADDRESS[1] = {0x04};
	protected:
	private:

	//functions
	public:
	Registers();
	~Registers();
	protected:
	private:
	Registers( const Registers &c );
	Registers& operator=( const Registers &c );

}; //Registers

#endif //__REGISTERS_H__
