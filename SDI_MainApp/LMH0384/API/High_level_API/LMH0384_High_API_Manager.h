/*
* LMH0384_High_API_Manager.h
*
* Created: 10/25/2016 7:00:18 PM
* Author: Roman
*/


#ifndef __LMH0384_HIGH_API_MANAGER_H__
#define __LMH0384_HIGH_API_MANAGER_H__

#include "../../Tools/Tools.h"
#include "../Low_level_API/LMH0384_Low_API_Manager.h"

class LMH0384_High_API_Manager
{
	//variables
	public:
	protected:

	private:
	LMH0384_Low_API_Manager *lowApiMgr;
	int chipID = -1;
	//functions
	public:
	LMH0384_High_API_Manager();
	~LMH0384_High_API_Manager();
	
	int Init(int chipID);
	
	

	/**
	* Checks if carrier is detected
	* @param res an unsigned integer (8 bit) pointer argument.
	* @return The error code
	*/
	int IsCarrierDetected(uint8_t* res);
	
	/**
	* Gets the length of a cable
	* @param res a double pointer argument.
	* @return The error code
	*/
	int GetCableLength(double* res);
	
	/**
	* Get the LMH0384 device ID
	* @param res an unsigned integer (8 bit) pointer argument.
	* @return The error code
	*/
	int GetDeviceId(uint8_t* res);
	
	/**
	* Control sleep mode
	* @param option an unsigned integer (8 bit) pointer argument.
	* 00 - disable
	* 01 - sleep mode's active when no input signal detected
	* 10 - force equqlizer into sleep mode
	* @return The error code
	*/
	int ControlSleepMode(uint8_t* option);
	
	/**
	* Checks if is bypassed (if the signal is equalized)
	* @param res an unsigned integer (8 bit) pointer argument.
	* @return The error code
	*/
	int IsBypassed(uint8_t* res);
	
	/**
	* Checks if outputs are muted
	* @param res an unsigned integer (8 bit) pointer argument.
	* @return The error code
	*/
	int IsMuted(uint8_t* res);
	
	/**
	* Launch amplitude optimization fine tuning
	* @param prctg an unsigned integer (8 bit) pointer argument - percentage.
	* 0000: Nominal.
	* 0001: -4% from nominal.
	* 0010: -8% from nominal.
	* 0011: -11% from nominal.
	* 0100: -14% from nominal.
	* 0101: -17% from nominal.
	* 0110: -20% from nominal.
	* 0111: -22% from nominal.
	* 1000: Nominal.
	* 1001: +4% from nominal.
	* 1010: +9% from nominal.
	* 1011: +14% from nominal.
	* 1100: +20% from nominal.
	* 1101: +26% from nominal.
	* 1110: +33% from nominal.
	* 1111: +40% from nominal.
	* @return The error code
	*/
	int TuneLaunchAmplitudeFine(uint8_t* prctg);
	
	/**
	* Coarse launch amplitude optimization
	* @param attOption an unsigned integer (8 bit) pointer argument - attenuation option.
	* 0: Normal optimization with no (800 mVP-P launch amplitude).
	* 1: Optimized for 6 dB external attenuation (400 mVP-P launch amplitude).
	* @return The error code
	*/
	int TuneLaunchAmplitudeCoarse(uint8_t* attOption);

	protected:
	private:
	
	/**
	* Low API manger initialization
	* @return The error code
	*/
	int InitLowAPI(int chipID);
	
}; //LMH0384_High_API_Manager

#endif //__LMH0384_HIGH_API_MANAGER_H__
