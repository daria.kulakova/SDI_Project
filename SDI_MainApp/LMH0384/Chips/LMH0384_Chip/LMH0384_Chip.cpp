/*
* ATxmega128A4U_Chip.cpp
* Author: Roman
*/


#include "LMH0384_Chip.h"

// default constructor
LMH0384_Chip::LMH0384_Chip()
: Base_Chip(chipID)
{
	lowApiMgr = new LMH0384_Low_API_Manager();
} //ATxmega128A4U_Chip

// default destructor
LMH0384_Chip::~LMH0384_Chip()
{
	delete lowApiMgr;
} //~ATxmega128A4U_Chip

int LMH0384_Chip::initChip (int chipID)
{
	// === High API is unneccessary yet ===
	//highApiMgr->InitLowAPI (this->chipID);
	
	lowApiMgr->Init_SPI (chipID);
	
	return 0;
}

int LMH0384_Chip::getChipID (int* buf)
{
	buf = &this->chipID;
	return 0;
}

int LMH0384_Chip::setChipID (int chipID)
{
	//this->chipID = chipID;
	lowApiMgr->Deselect_Device_By_ID(this->chipID);
	this->chipID = chipID;
	lowApiMgr->Select_Device_By_ID(chipID);
	return 0;
}

int LMH0384_Chip::readRegister (uint8_t *regAddress, uint8_t* buf)
{
	int error = 0 ;
	if(*regAddress == lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS[0])
	{
		error =lowApiMgr->Read_LMH0384_General_Control_Register (buf);
	}
	else if (*regAddress == lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS[0])
	{
		error =lowApiMgr->Read_LMH0384_Output_Driver_Register (buf);
	}
	else if(*regAddress == lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS[0])
	{
		error =lowApiMgr->Read_LMH0384_Launch_Amplitude_Register (buf);
	}
	else if (*regAddress == lowApiMgr->regs->CLI_REGISTER_ADDRESS[0])
	{
		error =lowApiMgr->Read_LMH0384_CLI_Register (buf);
	}
	else if (*regAddress == lowApiMgr->regs->DEVICE_ID_REGISTER_ADDRESS[0])
	{
		error = lowApiMgr->Read_LMH0384_DevID_Register (buf);
	}
	return error;
}

int LMH0384_Chip::readRegisterField (uint8_t* regAddress, uint8_t* field, uint8_t* buf)
{
	int error = 0;
	if(*regAddress == lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->CARRIER_DETECT[0])
		{
			error =lowApiMgr->Read_LMH0384_GnrlCtrl_CD (buf);
		}
		if(*field == lowApiMgr->regs->MUTE[0])
		{
			error =lowApiMgr->Read_LMH0384_GnrlCtrl_Mute (buf);
		}
		if(*field == lowApiMgr->regs->BYPASS[0])
		{
			error =lowApiMgr->Read_LMH0384_GnrlCtrl_Bypass (buf);
		}
		if(*field == lowApiMgr->regs->SLEEP_MODE[0])
		{
			error =lowApiMgr->Read_LMH0384_GnrlCtrl_SlpMode (buf);
		}
		if(*field == lowApiMgr->regs->EXTENDED_3G_REACH_MODE[0])
		{
			error =lowApiMgr->Read_LMH0384_GnrlCtrl_Extended_3G_reach_mode (buf);
		}
	}
	else if(*regAddress == lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->OUTPUT_SWING[0])
		{
			error =lowApiMgr->Read_LMH0384_OutputDriver_Output_swing (buf);
		}
		if(*field == lowApiMgr->regs->OFFSET_VOLTAGE[0])
		{
			error =lowApiMgr->Read_LMH0384_OutputDriver_Offset_voltage (buf);
		}
	}
	else if(*regAddress == lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->COARSE_CONTROL[0])
		{
			error =lowApiMgr->Read_LMH0384_LaunchAmpld_CoarseCtrl (buf);
		}
		if(*field == lowApiMgr->regs->FINE_CONTROL[0])
		{
			error =lowApiMgr->Read_LMH0384_LaunchAmpld_FineCtrl (buf);
		}
	}
	return error;
}

int LMH0384_Chip::writeRegister (uint8_t *regAddress, uint8_t* data)
{
	int error = 0;
	if(*regAddress == lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS[0])
	{
		error = lowApiMgr->Write_LMH0384_General_Control_Register (data);
	}
	else if (*regAddress == lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS[0])
	{
		error = lowApiMgr->Write_LMH0384_Output_Driver_Register (data);
	}
	else if(*regAddress == lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS[0])
	{
		error = lowApiMgr->Write_LMH0384_Launch_Amplitude_Register (data);
	}
	return error;
}

int LMH0384_Chip::writeRegisterField (uint8_t* regAddress, uint8_t* field, uint8_t* data)
{
	int error = 0;
	if(*regAddress == lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->CARRIER_DETECT[0])
		{
			error = lowApiMgr->Write_LMH0384_GnrlCtrl_CD (data);
		}
		if(*field == lowApiMgr->regs->MUTE[0])
		{
			error = lowApiMgr->Write_LMH0384_GnrlCtrl_Mute (data);
		}
		if(*field == lowApiMgr->regs->BYPASS[0])
		{
			error = lowApiMgr->Write_LMH0384_GnrlCtrl_Bypass (data);
		}
		if(*field == lowApiMgr->regs->SLEEP_MODE[0])
		{
			error = lowApiMgr->Write_LMH0384_GnrlCtrl_SlpMode (data);
		}
		if(*field == lowApiMgr->regs->EXTENDED_3G_REACH_MODE[0])
		{
			error = lowApiMgr->Write_LMH0384_GnrlCtrl_Extended_3G_reach_mode (data);
		}
	}
	else if(*regAddress == lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->OUTPUT_SWING[0])
		{
			error = lowApiMgr->Write_LMH0384_OutputDriver_Output_swing (data);
		}
		if(*field == lowApiMgr->regs->OFFSET_VOLTAGE[0])
		{
			error = lowApiMgr->Write_LMH0384_OutputDriver_Offset_voltage (data);
		}
	}
	else if(*regAddress == lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS[0])
	{
		if(*field == lowApiMgr->regs->COARSE_CONTROL[0])
		{
			error = lowApiMgr->Write_LMH0384_LaunchAmpld_CoarseCtrl (data);
		}
		if(*field == lowApiMgr->regs->FINE_CONTROL[0])
		{
			error = lowApiMgr->Write_LMH0384_LaunchAmpld_FineCtrl (data);
		}
	}
	return error;
}