/* 
* UnitTest.h
*
* Created: 11/10/2016 9:23:58 AM
* Author: darya
*/


#ifndef __UNITTEST_H__
#define __UNITTEST_H__
#include "../SDI_Manager/SDI_Manager.h"

class UnitTest
{
//variables
public:
protected:
private:
	SDI_Manager* sdiManager;

//functions
public:
	UnitTest();
	/*static*/ void Initialize();
	int TestRequstsToLMH0303();
	int TestRequstsToLMH0384();
	~UnitTest();
protected:
private:
	UnitTest( const UnitTest &c );
	UnitTest& operator=( const UnitTest &c );

}; //UnitTest

#endif //__UNITTEST_H__
