/* 
* TWI_Chip.cpp
*
* Created: 13.10.2016 13:59:32
* Author: darya
*/


#include "TWI_Chip.h"


	
TWI_Chip::TWI_Chip(int chipID)
{
	setChipID(chipID);
} //TWI_Chip

// default destructor
TWI_Chip::~TWI_Chip()
{
} //~TWI_Chip

int TWI_Chip::changeDefaultAddresses(){	
		uint8_t value = (TWI_SLAVE_1<<1);
		twi_package_t packet;
		packet.addr[0]		= 0x00;
		packet.addr_length	= sizeof(uint8_t);
		packet.chip			= 0x17;
		packet.buffer		= (void *)&value;
		packet.length		= sizeof(uint8_t);
		packet.no_wait		= false;				// Wait if bus is busy
		
		int error = 0;
		cpu_irq_enable();
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLDOWN_gc;
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLUP_gc;
		error = twi_master_write(&TWI_MASTER, &packet);
		
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLDOWN_gc;
		value = (TWI_SLAVE_2<<1);
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLUP_gc;
		error = twi_master_write(&TWI_MASTER, &packet);
		
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLDOWN_gc;
		value = (TWI_SLAVE_3<<1);
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLUP_gc;
		error = twi_master_write(&TWI_MASTER, &packet);
		
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLDOWN_gc;
		value = (TWI_SLAVE_4<<1);
		TWI_MASTER_PORT.PIN2CTRL = PORT_OPC_PULLUP_gc;
		error = twi_master_write(&TWI_MASTER, &packet);
		
		return error;
}

int TWI_Chip::getChipID(){
	return id;
}
void TWI_Chip::setChipID(int chipID){
	if(chipID>0 && chipID<5){
		id = chipID;
		address = id==1?TWI_SLAVE_1 : id==2?TWI_SLAVE_2 : id==3?TWI_SLAVE_3 : TWI_SLAVE_4;
	}	
}

uint8_t TWI_Chip::getChipAddress(){
	return address;
}

void TWI_Chip::Initialize(){
		//sysclk_init();
		//board_init();
		// Use the internal pullups for SDA and SCL
		TWI_MASTER_PORT.PIN0CTRL = PORT_OPC_WIREDANDPULL_gc;
		TWI_MASTER_PORT.PIN1CTRL = PORT_OPC_WIREDANDPULL_gc;
		
		twi_options_t m_options = {
			TWI_SPEED,
			TWI_BAUD(sysclk_get_cpu_hz(), TWI_SPEED),
			TWI_MASTER_ADDR
		};
		
		irq_initialize_vectors();
		// Initialize TWI_MASTER
		sysclk_enable_peripheral_clock(&TWI_MASTER);
		twi_master_init(&TWI_MASTER, &m_options);
		twi_master_enable(&TWI_MASTER);
}

int TWI_Chip::readR(uint8_t registerAddress, uint8_t* value){
		
	twi_package_t packet_received;
	packet_received.addr[0]		 = registerAddress;
	packet_received.addr_length  = sizeof (uint8_t);		// TWI slave memory address data size
	packet_received.chip         = address;					// TWI slave bus address
	packet_received.buffer       = value;					// transfer data destination buffer
	packet_received.length       = sizeof(uint8_t);			// transfer data size (bytes)
	packet_received.no_wait      = false;					// Wait if bus is busy

	// Perform a multi-byte read access then check the result.
	return twi_master_read(&TWI_MASTER, &packet_received);
}

int TWI_Chip::writeR(uint8_t registerAddress, uint8_t value){
	
	twi_package_t packet;
	packet.addr[0]		= registerAddress;
	packet.addr_length	= sizeof(uint8_t);
	packet.chip			= address;
	packet.buffer		= (void *)&value;
	packet.length		= sizeof(uint8_t);
	packet.no_wait		= false;				// Wait if bus is busy
	
	cpu_irq_enable();
	// Send package to slave
	return twi_master_write(&TWI_MASTER, &packet);
}