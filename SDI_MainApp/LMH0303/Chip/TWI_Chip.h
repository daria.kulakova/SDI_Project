/* 
* TWI_Chip.h
*
* The most low level code for reading the register of one slave.
*
* Created: 13.10.2016 13:59:32
* Author: darya
*/


#ifndef __TWI_CHIP_H__
#define __TWI_CHIP_H__

//#include <stdint.h>
#include <stdbool.h>
//#include <stdio.h>
//#include <string.h>
#include <asf.h>


#define TWI_MASTER           TWIE		
#define TWI_MASTER_PORT      PORTE
#define TWI_SPEED            50000
#define TWI_MASTER_ADDR      0x50


#define TWI_SLAVE_1          0x05		/* First slave address. */
#define TWI_SLAVE_2          0x06		/* Second slave address. */
#define TWI_SLAVE_3          0x07		/* Third slave address. */
#define TWI_SLAVE_4          0x08		/* Fourth slave address. */

/*!
 * \brief Class of slave for reading from/writing to LMH0303. Low level.
 */
class TWI_Chip
{
//variables
public:
protected:
private:
	int id;					/**<ID of slave we work with. */
	uint8_t address;		/**<Address of slave we work with. Depends on ID. */


//functions
public:
	/**
	* Get ID of selected chip
	* @return ID of selected chip(from 1 to 4)
	*/
	int getChipID();
	/**
	* Set ID of the chip
	* @param chipID must be from 1 to 4
	* @return Nothing
	*/
	void setChipID(int chipID);
	/**
	* Get an address of selected chip
	* @return An address of selected chip
	*/
	uint8_t getChipAddress();
	
	/**
	* Initialize TWI_Chip class. Should be called before using current class.
	*/	
	static void Initialize();
	/**
	* Read the register on address "reg" fully(8 bits) to the param "val"
	* @param reg address of selected register
	* @param value should refer to the value of the register we have read
	* @return Number of an error or 0 if there is no error. -10 if no such register
	*/	
	int readR(uint8_t reg, uint8_t* val);
	/**
	* Write value of the param "val" to the register on address fully(8 bits)
	* @param reg address of selected register
	* @param value should refer to the value of the register we have read
	* @return Number of an error or 0 if there is no error. -10 if no such register
	*/		
	int writeR(uint8_t reg, uint8_t val);
	/**
	* Called when all LMH0303 first come up and have the similar default addresses
	* @return Nothing
	*/	
	static int changeDefaultAddresses();
	/**
	* Create an object of TWI_Chip class with chipID "id" and appropriate address
	*/		
	TWI_Chip(int id);
	~TWI_Chip();
protected:
private:
	TWI_Chip( const TWI_Chip &c );
	TWI_Chip& operator=( const TWI_Chip &c );

}; //TWI_Chip

#endif //__TWI_CHIP_H__
