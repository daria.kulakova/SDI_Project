/*
 * TWI_Manager.h
 *
 * Created: 12.10.2016 14:59:44
 *  Author: darya
 */ 


#ifndef TWI_MANAGER_H_
#define TWI_MANAGER_H_


//#include <stdint.h>
/*
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <asf.h>*/
//#include <conf_test.h>
#include "../Chip/registers.h"
//#include "Chip.h"
#include "../Chip/TWI_Chip.h"
#include "../new/new.h"

using namespace std;

/*!
 * \brief Class of the manager for reading from/writing to LMH0303 using TWI_Chip. High level.
 */
class LMH0303_Manager
{
	//variables
	public:	
	protected:
	private:		
		TWI_Chip* chip;		/**<Object for reading from/writing to LMH0303. */


	//functions
	public:
	/**
	* Create LMH0303_Manager object working with slave on ID
	* @param chipID ID of slave manager work with
	*/	
	LMH0303_Manager(int chipID);
	~LMH0303_Manager();
	/**
	* Get ID of selected chip
	* @return The error code
	*/
	int getChipID();
	/**
	* Set ID of the chip
	* @param chipID must be from 1 to 4
	*/	
	void setChipID(int chipID);
	/**
	* Get an address of selected chip
	* @return An address of selected chip
	*/	
	uint8_t getChipAddress();
	
	/**
	* Initialize LMH0303_Manager class. Should be called before using current class.
	*/	
	static void Initialize();
	
	/*************************** L O W    L E V E L    A P I **********************************/

	/**
	* Read the selected register fully(8 bits) to the param "value"
	* @param reg selected register from the enum "register"
	* @see "registers.h"
	* @param value should refer to the value of the register we have read
	* @return Number of an error or 0 if there is no error. -10 if no such register
	*/
	int readRegister(Register::Name reg, uint8_t* value);

	/**
	* Write value of the param "value" to the selected register fully(8 bits)
	* @param reg selected Register from the enum "Register"
	* @param value value we write to the selected register
	* @return Number of an error or 0 if there is no error. -10 if no such register. -9 if register is read only
	*/
	int writeRegister(Register::Name reg, uint8_t value);



	/*************************** H I G H    L E V E L    A P I **********************************/

	/**
	* Read value to the param "value" from the selected "part" of register "reg"
	* @param reg selected register from the enum in namespace "Register"
	* @see "registes.h"
	* @param part must be an item of enum in namespace: DeviceIDRegister, StatusRegister, MaskRegister, DirectionRegister, OutputRegister, OutputCTRLRegister, RSVD06Register, RSVD07Register, TestRegister, RevRegister, TFPCountRegister, TFNCountRegister
	* @param value reference to a value we read to from the selected part of selected register
	* @return Number of an error or 0 if there is no error. -10 if no such register. -8 if no such part in a selected register
	*/
	int getRegisterField(Register::Name reg, int part, uint8_t* value);
	
	/**
	* Write value of the param "value" to the selected "part" of register "reg"
	* @param reg selected register from the enum in namespace "Register"
	* @see "registes.h"
	* @param part must be an item of enum in namespace: DeviceIDRegister, StatusRegister, MaskRegister, DirectionRegister, OutputRegister, OutputCTRLRegister, RSVD06Register, RSVD07Register, TestRegister, RevRegister, TFPCountRegister, TFNCountRegister
	* @param value value we write to the selected part of selected register
	* @return Number of an error or 0 if there is no error. -10 if no such register. -9 if register is read only. -8 if no such part in a selected register
	*/
	int setRegisterField(Register::Name reg, int part, uint8_t value);

	protected:
	private:
	int getRField(Register::Name reg, uint8_t* value, uint8_t forAND, int shift);
	int setRField(Register::Name reg, uint8_t value, uint8_t forAND, int shift);
	int getStatusRegisterField(StatusRegister::Field part, uint8_t* value);
	int getMaskRegisterField(MaskRegister::Field part, uint8_t* value);
	int getDirectionRegisterField(DirectionRegister::Field part, uint8_t* value);
	int getOutputRegisterField(OutputRegister::Field part, uint8_t* value);
	int getOutputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t* value);
	int setStatusRegisterField(StatusRegister::Field part, uint8_t value);
	int setMaskRegisterField(MaskRegister::Field part, uint8_t value);
	int setDirectionRegisterField(DirectionRegister::Field part, uint8_t value);
	int setOutputRegisterField(OutputRegister::Field part, uint8_t value);
	int setOutputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t value);
	
	int statusRegisterField(StatusRegister::Field part, uint8_t& forAND, int& shift);
	int maskRegisterField(MaskRegister::Field part, uint8_t& forAND, int& shift);
	int directionRegisterField(DirectionRegister::Field part, uint8_t& forAND, int& shift);
	int outputRegisterField(OutputRegister::Field part, uint8_t& forAND, int& shift);
	int outputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t& forAND, int& shift);
	LMH0303_Manager( const LMH0303_Manager &c );
	LMH0303_Manager& operator=( const LMH0303_Manager &c );

}; //LMH0303_Manager




#endif /* TWI_MANAGER_H_ */