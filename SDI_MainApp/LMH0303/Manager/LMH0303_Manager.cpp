/*
 * TWI_Manager.cpp
 *
 * Created: 12.10.2016 15:00:02
 *  Author: darya
 */ 

#include "LMH0303_Manager.h"

/*

void * operator new(size_t size)
{
	return malloc(size);
}

void operator delete(void * ptr)
{
	free(ptr);
}
*/


// default constructor
LMH0303_Manager::LMH0303_Manager(int id)
{
	chip = new TWI_Chip(id);
} //LMH0303_Manager

// default destructor
LMH0303_Manager::~LMH0303_Manager()
{
	delete chip;
} //~LMH0303_Manager

void LMH0303_Manager::Initialize(){
	TWI_Chip::Initialize();
}

int LMH0303_Manager::getChipID(){
	return chip->getChipID();
}

void LMH0303_Manager::setChipID(int id){
	chip->setChipID(id);	
}

uint8_t LMH0303_Manager::getChipAddress(){
	return chip->getChipAddress();
}


int LMH0303_Manager::writeRegister(Register::Name reg, uint8_t value){
	int error = 0;
	switch(reg){
		case Register::DeviceID:
			value &= 0xFE;		// 0xFE = 11111110
			error = chip->writeR(0x00, value);
			break;
		case Register::Status:
			error = -9;
			break;
		case Register::Mask:
			value &= 0xA7;		// 0xA7 = 10100111
			error = chip->writeR(0x02, value);
			break;
		case Register::Direction:
			value &= 0xC7;		// 0xC7 = 11000111
			error = chip->writeR(0x03, value);
			break;
		case Register::Output:
			error = chip->writeR(0x04, value);
			break;
		case Register::OutputCTRL:
			value &= 0x7F;		// 0x7F = 01111111
			error = chip->writeR(0x05, value);
			break;
		case Register::RSVD06:
			error = -9;
			break;
		case Register::RSVD07:
			error = -9;
			break;
		case Register::Test:
			value &= 0xE0;		// 0xE0 = 11100000
			error = chip->writeR(0x08, value);
			break;
		case Register::Rev:
			error = -9;
			break;
		case Register::TFPCount:
			error = -9;
			break;
		case Register::TFNCount:
			error = -9;
			break;
		default:
			error = -10;
			break;
	}
	return error;
}

int LMH0303_Manager::readRegister(Register::Name reg, uint8_t* value){
	int error = 0;
	switch(reg){
		case Register::DeviceID:
		error = chip->readR(0x00, value);
		break;
		case Register::Status:
		error = chip->readR(0x01, value);
		break;
		case Register::Mask:
		error = chip->readR(0x02, value);
		break;
		case Register::Direction:
		error = chip->readR(0x03, value);
		break;
		case Register::Output:
		error = chip->readR(0x04, value);
		break;
		case Register::OutputCTRL:
		error = chip->readR(0x05, value);
		break;
		
		case Register::RSVD06:
		error = chip->readR(0x06, value);
		break;
		case Register::RSVD07:
		error = chip->readR(0x07, value);
		break;
		
		case Register::Test:
		error = chip->readR(0x08, value);
		break;
		case Register::Rev:
		error = chip->readR(0x09, value);
		break;
		case Register::TFPCount:
		error = chip->readR(0x0A, value);
		break;
		case Register::TFNCount:
		error = chip->readR(0x0B, value);
		break;
		default:
		error = -10;
		break;
	}
	return error;
}


int LMH0303_Manager::getRField(Register::Name reg, uint8_t* mainValue, uint8_t forAND, int shift){
	uint8_t registerValue;
	int error = 0;
	
	error = readRegister(reg, &registerValue);
	//if(error < 0) return error;
	
	*mainValue = (registerValue & forAND)>>shift;
	
	return error;
}

int LMH0303_Manager::setRField(Register::Name reg, uint8_t mainValue, uint8_t forAND, int shift){
	uint8_t registerValue;
	uint8_t oldRegisterValue;
	int error = 0;
	
	error = readRegister(reg, &oldRegisterValue);
	if(error < 0) return error;
	
	registerValue = ((mainValue<<shift) & forAND) | oldRegisterValue;
	error = writeRegister(reg, registerValue);
	if(error < 0) return error;
	
	return error;
}


int LMH0303_Manager::statusRegisterField(StatusRegister::Field part, uint8_t& forAND, int& shift){
	int error = 0;
	switch(part){
		case StatusRegister::RSVD:
			forAND =  0xF8; // 0xF8 = 11111000
			shift = 3;
			break;
		case StatusRegister::TFN :
			forAND =  0x04; // 0x04 = 00000100
			shift = 2;
			break;
		case StatusRegister::TFP :
			forAND =  0x02; // 0x02 = 00000010
			shift = 1;
			break;
		case StatusRegister::LOS :
			forAND =  0x01; // 0x01 = 00000001
			shift = 0;
			break;
		default:
			error = -8;
			break;
	}
	return error;
}
int LMH0303_Manager::maskRegisterField(MaskRegister::Field part, uint8_t& forAND, int& shift){
	int error = 0;
	switch(part){
		case MaskRegister::SD:
			forAND =  0x80; // 0x80 = 10000000
			shift = 7;
			break;
		case MaskRegister::RSVD6:
			forAND =  0x40; // 0x40 = 01000000
			shift = 6;	
			break;		
		case MaskRegister::PD :
			forAND =  0x20; // 0x20 = 00100000
			shift = 5;
			break;
		case MaskRegister::RSVD43:
			forAND =  0x18; // 0x18 = 00011000
			shift = 3;
			break;			
		case MaskRegister::MTFN :
			forAND =  0x04; // 0x04 = 00000100
			shift = 2;
			break;
		case MaskRegister::MTFP :
			forAND =  0x02; // 0x02 = 00000010
			shift = 1;
			break;
		case MaskRegister::MLOS :
			forAND =  0x01; // 0x01 = 00000001
			shift = 0;
			break;
		default:
			error = -8;
			break;
	}
	return error;
}
int LMH0303_Manager::directionRegisterField(DirectionRegister::Field part, uint8_t& forAND, int& shift){
	int error = 0;
	switch(part){
		case DirectionRegister::HDTFThreshLSB:
			forAND =  0x80; // 0x80 = 10000000
			shift = 7;
			break;
		case DirectionRegister::SDTFThreshLSB :
			forAND =   0x40; // 0x40 = 01000000
			shift = 6;
			break;
		case DirectionRegister::RSVD:
			forAND =   0x38; // 0x38 = 00111000
			shift = 3;		
			break;
		case DirectionRegister::DTFN:
			forAND =  0x04; // 0x04 = 00000100
			shift = 2;
			break;
		case DirectionRegister::DTFP:
			forAND =  0x02; // 0x02 = 00000010
			shift = 1;
			break;
		case DirectionRegister::DLOS:
			forAND =  0x01; // 0x01 = 00000001
			shift = 0;
			break;
		default:
			error = -8;
			break;
	}
	return error;
}
int LMH0303_Manager::outputRegisterField(OutputRegister::Field part, uint8_t& forAND, int& shift){
	int error = 0;
	switch(part){
		case OutputRegister::HDTFThresh:
			forAND =  0xE0; // 0xE0 = 11100000
			shift = 5;
			break;
		case OutputRegister::AMP:
			forAND =   0x1F;  // 0x1F = 00011111
			shift = 0;
			break;
		default:
			error = -8;
			break;
	}
	return error;
}
int LMH0303_Manager::outputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t& forAND, int& shift){
	int error = 0;
	switch(part){
		case OutputCTRLRegister::RSVD:
			forAND =  0x80; // 0x80 = 10000000
			shift = 6;		
			break;
		case OutputCTRLRegister::FLOSOF:
			forAND =  0x40; // 0x40 = 01000000
			shift = 6;
			break;
		case OutputCTRLRegister::FLOSON :
			forAND =   0x20; // 0x20 = 00100000
			shift = 5;
			break;
		case OutputCTRLRegister::LOSEN:
			forAND =  0x10;  // 0x10 = 00010000
			shift = 4;
			break;
		case OutputCTRLRegister::MUTE:
			forAND =  0x08;  // 0x08 = 00001000
			shift = 3;
			break;
		case OutputCTRLRegister::SDTFThresh:
			forAND =  0x07; // 0x07 = 00000111
			shift = 0;
			break;
		default:
			error = -8;
			break;
	}
	return error;
}

int LMH0303_Manager::getStatusRegisterField(StatusRegister::Field part, uint8_t* value){
	uint8_t forAND;
	int shift;
	int error = statusRegisterField(part, forAND, shift);
	if(error<0) return error;
	error = getRField(Register::Status, value, forAND, shift);
	if(error<0) return error;
	return 0;
}
int LMH0303_Manager::getMaskRegisterField(MaskRegister::Field part, uint8_t* value){
		uint8_t forAND;
		int shift;
		int error = maskRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = getRField(Register::Mask, value, forAND, shift);
		if(error<0) return error;
		return 0;
}
int LMH0303_Manager::setMaskRegisterField(MaskRegister::Field part, uint8_t value){
		uint8_t forAND;
		int shift;
		int error = maskRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = setRField(Register::Mask, value, forAND, shift);
		if(error<0) return error;
		return 0;
}
int LMH0303_Manager::getDirectionRegisterField(DirectionRegister::Field part, uint8_t* value){
		uint8_t forAND;
		int shift;
		int error = directionRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = getRField(Register::Direction, value, forAND, shift);
		if(error<0) return error;
		return 0;	
}
int LMH0303_Manager::setDirectionRegisterField(DirectionRegister::Field part, uint8_t value){
		uint8_t forAND;
		int shift;
		int error = directionRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = setRField(Register::Direction, value, forAND, shift);
		if(error<0) return error;
		return 0;
}
int LMH0303_Manager::getOutputRegisterField(OutputRegister::Field part, uint8_t* value){
		uint8_t forAND;
		int shift;
		int error = outputRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = getRField(Register::Output, value, forAND, shift);
		if(error<0) return error;
		return 0;	
}
int LMH0303_Manager::setOutputRegisterField(OutputRegister::Field part, uint8_t value){
		uint8_t forAND;
		int shift;
		int error = outputRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = setRField(Register::Output, value, forAND, shift);
		if(error<0) return error;
		return 0;	
}
int LMH0303_Manager::getOutputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t* value){
		uint8_t forAND;
		int shift;
		int error = outputCTRLRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = getRField(Register::OutputCTRL, value, forAND, shift);
		if(error<0) return error;
		return 0;	
}
int LMH0303_Manager::setOutputCTRLRegisterField(OutputCTRLRegister::Field part, uint8_t value){
		uint8_t forAND;
		int shift;
		int error = outputCTRLRegisterField(part, forAND, shift);
		if(error<0) return error;
		error = setRField(Register::OutputCTRL, value, forAND, shift);
		if(error<0) return error;
		return 0;	
}


int LMH0303_Manager::getRegisterField(Register::Name reg, int part, uint8_t* value){
	int error = 0;
	switch(reg){
		case Register::DeviceID:
			error = -8;
			if(part == DeviceIDRegister::DevID)		error = getRField(reg, value, 0xFE, 1); // 0xFE = 11111110
			else if(part == DeviceIDRegister::RSVD)  error = getRField(reg, value, 0x01, 0); // 0x01 = 00000001
			break;
		case Register::Status:
			error = getStatusRegisterField((StatusRegister::Field)part, value);
			break;
		case Register::Mask:
			error = getMaskRegisterField((MaskRegister::Field)part, value);
			break;
		case Register::Direction:
			error = getDirectionRegisterField((DirectionRegister::Field)part, value);
			break;
		case Register::Output:
			error = getOutputRegisterField((OutputRegister::Field)part, value);
			break;
		case Register::OutputCTRL:
			error = getOutputCTRLRegisterField((OutputCTRLRegister::Field)part, value);
			break;
		case Register::RSVD06:
			error = -8;
			if(part == RSVD06Register::RSVD)	error = chip->readR(0x06, value);
			break;
		case Register::RSVD07:
			error = -8;
			if(part == RSVD07Register::RSVD)	error = chip->readR(0x07, value);
			break;
		case Register::Test:
			error = -8;
			if(part == TestRegister::CMPCMD) 		error = getRField(reg, value, 0xE0, 5); // 0xE0 = 11100000
			else if(part == TestRegister::RSVD)		error = getRField(reg, value, 0x1F, 0); // 0x1F = 00011111
			break;
		case Register::Rev:
			error = -8;
			if(part == RevRegister::RSVD)		error = getRField(reg, value, 0xE0, 5); // 0xE0 = 11100000
			else if(part == RevRegister::DIREV)		error = getRField(reg, value, 0x18, 3); // 0x18 = 00011000
			else if(part == RevRegister::PARTID)			error = getRField(reg, value, 0x07, 0); // 0x07 = 00000111
			break;
		case Register::TFPCount:
			error = -8;
			if(part == TFPCountRegister::RSVD)		error = getRField(reg, value, 0xE0, 5); // 0xE0 = 11100000
			else if(part == TFPCountRegister::TFPCount)	error = getRField(reg, value, 0x1F, 0); // 0x1F = 00011111
			break;
		case Register::TFNCount:
			error = -8;
			if(part == TFNCountRegister::RSVD)		error = getRField(reg, value, 0xE0, 5); // 0xE0 = 11100000
			else if(part == TFNCountRegister::TFNCount)	error = getRField(reg, value, 0x1F, 0); // 0x1F = 00011111
			break;
		default:
			error = -10;
			break;
	}
	return error;
}
int LMH0303_Manager::setRegisterField(Register::Name reg, int part, uint8_t value){
	int error = 0;
	switch(reg){
		case Register::DeviceID:
			error = -8;
			if(part == DeviceIDRegister::DevID)		error = setRField(reg, value, 0xFE, 1); // 0xFE = 11111110
			break;
		case Register::Status:
			error = -9;
			break;
		case Register::Mask:
			error = setMaskRegisterField((MaskRegister::Field)part, value);
			break;
		case Register::Direction:
			error = setDirectionRegisterField((DirectionRegister::Field)part, value);
			break;
		case Register::Output:
			error = setOutputRegisterField((OutputRegister::Field)part, value);
			break;
		case Register::OutputCTRL:
			error = setOutputCTRLRegisterField((OutputCTRLRegister::Field)part, value);
			break;
		case Register::RSVD06:
			error = -8;
			if(part == RSVD06Register::RSVD)	error = chip->writeR(0x06, value);
			break;
		case Register::RSVD07:
			error = -8;
			if(part == RSVD07Register::RSVD)	error = chip->writeR(0x07, value);
			break;
		case Register::Test:
			error = -8;
			if(part == TestRegister::CMPCMD) 	error = setRField(reg, value, 0xE0, 5); // 0xE0 = 11100000
			break;
		case Register::Rev:
			error = -9;
			break;
		case Register::TFPCount:
			error = -9;
			break;
		case Register::TFNCount:
			error = -9;
			break;
		default:
			error = -10;
			break;
	}
	return error;
}